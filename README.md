# Starhopper Analytics

This Jenkins pipeline involve the below steps:

1) Validating the pre-flight check
2) Build and push image with Container Builder
3) Helm Charts for deployment in GKE cluster

The agent of Jenkins Pipeline used Kubernetes for creating Container template and Container builder.

The containers contain the image for gcloud, kubectl and Helm

1) Validating pre-flight check
- It will validate all the required tool for this pipeline

2) Build and push image with Container Builder
- Container Builder will build and push the images to GCR using gcloud
- imageRepo and imageTag has been defined as environment variable and the tag will increment with every build

3) Helm Chart for deployment
- Helm Chart is being used define, install and upgrade the kubernetes application

helmcharts/starhopper-main/
  Chart.yaml: A YAML file containing brief information about the starhopper-analytics chart
  README.md: A human-readable README file about chart structure
  values.yaml: The default configuration values for the starhopper-analytics chart. It provide the default values for Deployment, service and ingress yaml file.
  templates/: A directory of templates that, when combined with values, will generate valid Kubernetes manifest files.
  templates/Deployment.yml: Contain the deployment info such as imageRepo, imageTag, labels, replicaCount and so on.
  template/service.yml: It create service for the existing deployment
  template/ingress.yml: Ingress is used to expose to outside world by creating load balancer.

# Install Helm and Tiller on GKE cluster

1) Download and install the Helm binary:

wget https://storage.googleapis.com/kubernetes-helm/helm-v2.14.1-linux-amd64.tar.gz

2) Unzip the file to your local system:

tar zxfv helm-v2.14.1-linux-amd64.tar.gz
cp linux-amd64/helm .

3) Add yourself as a cluster administrator in the cluster's RBAC so that you can give Jenkins permissions in the cluster:

kubectl create clusterrolebinding cluster-admin-binding --clusterrole=cluster-admin \
        --user=$(gcloud config get-value account)
4) Grant Tiller, the server side of Helm, the cluster-admin role in your cluster:

kubectl create serviceaccount tiller --namespace kube-system
kubectl create clusterrolebinding tiller-admin-binding --clusterrole=cluster-admin \
    --serviceaccount=kube-system:tiller

5) Initialize Helm. This ensures that the Tiller is properly installed in your cluster.

./helm init --service-account=tiller
./helm repo update

6) Ensure Helm is properly installed by running the following command:

./helm version

# Restart the Jenkins on new port if required

export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/component=jenkins-master" -l "app.kubernetes.io/instance=cd" -o jsonpath="{.items[0].metadata.name}")
kubectl port-forward $POD_NAME 8080:8080 >> /dev/null &

